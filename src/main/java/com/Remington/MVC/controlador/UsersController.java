package com.Remington.MVC.controlador;

import com.Remington.MVC.modelo.Usuarios;
import com.Remington.MVC.servicio.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

@Controller
public class UsersController {
    @Autowired
    UsersService userService;

    @GetMapping ({"/","/VerUsuarios"})
    public String users(Model model, @ModelAttribute("message") String message){
        List<Usuarios> AllUsers= userService.getAllUsers();
        model.addAttribute("AllUsers",AllUsers);
        model.addAttribute("message",message);
        return "verUsuarios";
    }

    @GetMapping("/EditarUsuario/{id}")
    public String modifyUser(Model model, @PathVariable Integer id, @ModelAttribute("message") String message){
        Optional<Usuarios> user= userService.getUserById(id);
        model.addAttribute("user",user);
        model.addAttribute("mensaje", message);
        return "editarUsuario";
    }

    @GetMapping("/AgregarUsuario")
    public String newUser(Model model, @ModelAttribute("message") String message){
        Usuarios user= new Usuarios();
        model.addAttribute("user",user);
        model.addAttribute("message",message);
        return "agregarUsuario";
    }

    @PostMapping("/GuardarUsuario")
    public String saveUser(Usuarios user, RedirectAttributes redirectAttributes){
        if(userService.dataUser(user)==true){
            redirectAttributes.addFlashAttribute("message","saveOK");
            return "redirect:/VerUsuarios";
        }
        redirectAttributes.addFlashAttribute("message","saveError");
        return "redirect:/AgregarUsuario";
    }

    @PostMapping("/ActualizarUsuario")
    public String updateUser(@ModelAttribute("company") Usuarios user, RedirectAttributes redirectAttributes){
        if(userService.dataUser(user)){
            redirectAttributes.addFlashAttribute("message","updateOK");
            return "redirect:/VerUsuarios";
        }
        redirectAttributes.addFlashAttribute("message","updateError");
        return "redirect:/EditarUsuario";

    }

    @GetMapping("/EliminarUsuario/{id}")
    public String deleteUser(@PathVariable Integer id, RedirectAttributes redirectAttributes){
        if (userService.deleteUser(id)==true){
            redirectAttributes.addFlashAttribute("message","deleteOK");
            return "redirect:/VerUsuarios";
        }
        redirectAttributes.addFlashAttribute("message", "deleteError");
        return "redirect:/VerUsuarios";
    }


}
