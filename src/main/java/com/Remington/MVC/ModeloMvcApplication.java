package com.Remington.MVC;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;


@SpringBootApplication (exclude ={SecurityAutoConfiguration.class})



public class ModeloMvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(ModeloMvcApplication.class, args);
	}

}
