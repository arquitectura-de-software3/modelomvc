package com.Remington.MVC.repositorio;

import com.Remington.MVC.modelo.Usuarios;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<Usuarios, Integer> {
}
