package com.Remington.MVC.servicio;

import com.Remington.MVC.modelo.Usuarios;
import com.Remington.MVC.repositorio.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UsersService {
    @Autowired
    UsersRepository usersRepository;
    //Obtener todos los usuarios
    public List<Usuarios> getAllUsers(){
        List<Usuarios> allUsers = new ArrayList<>();
        usersRepository.findAll().forEach(usuario -> allUsers.add(usuario));
        return allUsers;
    }
    //Obtener un usuario por id
    public Optional<Usuarios> getUserById(Integer id){
        Optional<Usuarios>userById= usersRepository.findById(id);
        userById.orElseThrow(()->new IllegalArgumentException ("Could not find user"));
        return userById;
    }

    //Crear o actualizar un usuario
    public boolean dataUser(Usuarios user){
        Usuarios usuario= usersRepository.save(user);
        if (usersRepository.findById(usuario.getId())!=null){
            return true;
        }
        return false;
    }
    //eliminar un usuario
    public Boolean deleteUser(Integer idUser) {
        Optional<Usuarios> findUser =
                usersRepository.findById(idUser);
        findUser.orElseThrow(() -> new IllegalArgumentException("not found User"));
        usersRepository.delete(findUser.get());
        return true;
    }


}
